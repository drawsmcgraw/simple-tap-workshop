# Simple Tap Workshop

Simple static website to assist with running Tanzu App Platform workshops

# Runs on TAP
This workshop is made with Hugo and has already been built. Use the [workload.yml](/tap/workload.yml) file to deploy.

# Updating Links
If you want the links to various parts of TAP to work, you'll need to update the "customize for your environment" section in the [config.toml](config.toml) and rebuild (see build instructions below).

# Building/Editing
To build onto this workshop, you'll need the `hugo` CLI and a clone of this repo. This workshop uses the [relearn theme](https://github.com/McShelby/hugo-theme-relearn) so you'll need to use the `--include-submodules` flag when cloning.

When you're happy with your content, run `hugo -D` to generate a new version of the content for the `public` directory, and you're done.

Share, and enjoy!

## Linkouts to relevant docs
[RBAC docs](https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.5/vmware-tanzu-kubernetes-grid-15/GUID-iam-configure-rbac.html#configure-rbac-for-a-workload-cluster-6) on the VMware TKG site.

## TODO
A list of ongoing improvements we'd like to see.
- use the `tanzu login` experience to log into k8s clusters instead of sharing a kubeconfig
- Add a discussion on Build Service
- Add the tiltfile experience
- Add a discussion of Convention service
- Data service binding