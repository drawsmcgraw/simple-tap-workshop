## Tanzu Application Service Workshop

This is a workshop with the Tanzu Application Platform, intended to give you hands-on with the most common features and give you confidence that you can deploy your own applications to the platform.


## Requirements
To use this workshop, you will need:
1. Access to a TAP installation
1. Your own Github/Gitlab repo (we will be forking sample applications into your own account)
1. The `tanzu` CLI (Download link [here](http://tap-workshop-downloads.s3-website.us-east-2.amazonaws.com/tanzu-cli/tanzu-framework-linux-amd64.tar) for Linux or [here](http://tap-workshop-downloads.s3-website.us-east-2.amazonaws.com/tanzu-cli/tanzu-framework-darwin-amd64.tar) for Mac)
1. A Linux workstation
1. A Browser

## Installing the tanzu CLI
Download and unpack the tarball. We'll assume you downloaded the file into the directory `~/tanzu`.

Once unpacked, cd into `~/tanzu` and:
```sh
# move the tanzu CLI onto your PATH and change executable bit
sudo cp cli/core/v0.11.6/tanzu-core-linux_amd64 /usr/local/bin/tanzu
sudo chmod 755 /usr/local/bin/tanzu

# install necessary plugins
tanzu plugin install all -l ~/tanzu/cli/
tanzu plugin sync
```
