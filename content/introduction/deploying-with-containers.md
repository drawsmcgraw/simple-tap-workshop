---
title: "Deploying with Containers"
date: 2022-08-03T16:46:34-04:00
draft: false
---

> Welcome to the Cloud. You are your own sysadmin now.
>
> > Someone on the Internet

When we deploy our code with containers, we've become accustomed to the following workflow.

1. Create a container image
1. Launch the container.
![docker run](/images/intro-01.png)

This was the 'primordial' state for containers when we all started on Docker. As our needs progressed, we started to develop a need to orchestrate our containers. This was usually done with something like `docker-compose` or similar. And we expanded to:

1. Create a container image
1. Write a `docker-compose` (or similar) file.
1. Launch the container(s).
![docker-compose up](/images/intro-02.png)

As we continued progressing (and as market conditions changed on us), we eventually developed the need to coordinate nontrivial container loads across a number of machines we don't even know will exist yet. This effectively became a sort of "conatiner orchestration fabric". Fast-forward ten years, and we now have Kubernetes. Massive power. But equally massive responsibility.

Let's revisit our workflow now, with Kubernetes.

1. Create a container image
1. Push our container image to a registry (you do have a registry, don't you?)
1. Write a Kubernetes manifest file (for pods, deployments, services, volumes, secrets, ingressControllers, etc)
1. [Optionally] Save those manifest files as a new software project, either as part of our original software project or separate.
1. Deploy our code with a `kubectl` command.
![kubectl something](/images/intro-03.png)

Hopefully the point is clear - as we've progressed in our needs for more complex container orchestration, so has the burden on our developers who _just want to get code into Kubernetes_. The Kubernetes ecosystem has become so complex, that just the act of reliably getting your code to run into Kubernetes has a certification associated with it (Certified Kubernetes Application Developer, or CKAD).

Things have changed so much, that we have to update our tagline.

> Welcome to Kubernetes. Your developers are also system engineers now.
>
> > Someone else on the Internet

Now that we have an understanding of the work involved to deploy our code onto Kubernetes, let's introduce the App Platform and how it helps us with some of this work.