+++
archetype = "chapter"
title = "Introduction"
weight = 1
+++

In this section, we will introduce the typical container workflow when deploying to Kubernetes. Then show how the workflow is simplified with TAP.
