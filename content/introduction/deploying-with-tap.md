---
title: "Deploying with TAP"
date: 2022-08-03T16:46:22-04:00
draft: false
---

Now that we understand the workflow involved with getting our code on Kubernetes, let's look at the same workflow, but this time with the Tanzu App Platform (TAP).

1. tanzu app workload create -f workload.yml

![filter by pod](/images/vault-boy.jpg?classes=border,shadow&width=10pc)

That's it! 

**Wait, but what happens?**

When you run this command, you're not really _uploading_ your code to the platform so much as you're _telling_ the platform where to find your code.

The rest of this workshop is dedicated to going through the details of how this works and what you can do with it.
