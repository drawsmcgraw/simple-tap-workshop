+++
archetype = "chapter"
title = "Additional Features"
weight = 4
+++

Let's explore some of the extra features of TAP, like Dockerfile support and how TAP makes microservice development simpler by making it easy to share API's across the team.