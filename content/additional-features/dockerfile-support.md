---
title: "Dockerfile Support"
date: 2022-08-17T13:49:46-04:00
draft: false
weight: 1
---

While the Platform is able to build your application container image for you using Cloud Native Buildpacks, we realize that not all apps are best suited to this. Additionally, you may already have a number of containerized apps and a lot of equity in those Dockerfiles. Let's explore what it's like to build an app by specifying a Dockerfile.

## Clone and Deploy
Deploying with a Dockerfile is just about identical to deploying without one. We'll still need a `workload.yaml` and (this time), we'll need a Dockerfile.

Let's try [this app](https://github.com/drawsmcgraw/python-test). We can fork this repo, clone it, etc. But really, we just need the `workload.yaml` from there, which we'll replicate here:
```yml
apiVersion: carto.run/v1alpha1
kind: Workload
metadata:
  name: python-test-from-dockerfile
  labels:
    apps.tanzu.vmware.com/workload-type: web
    app.kubernetes.io/part-of: python-accelerator
  annotations:
    autoscaling.knative.dev/minScale: "1"
spec:
  params:
  - name: dockerfile
    value: ./Dockerfile
  source:
    git:
      url: https://github.com/drawsmcgraw/python-test.git
      ref:
        branch: main
```

Using this file, we can simply deploy our app just like the last one.
```sh
tanzu -n $USERNAME app workload create -f workload-from-dockerfile.yaml
```

And that's it! We simply supply a parameter to our workload, giving it the path to the Dockerfile in the repo (in this case, the root of the repo) and the deployment is the same.