---
title: "Api Portal"
date: 2022-08-17T13:49:56-04:00
draft: false
weight: 2
---

## Microservices, Am I Right?

>You can't talk about Kubernetes without talking about microservices.
> > The author

One of the most challenging parts of microservices is coordinating the various APIs that each service implements. When a developer wants to interface with their peer's work, there are a number of ways of finding out what those software contracts look like:
- wikis
- Confluence pages
- documentation (hah!)
- emailing the app's owner (yes, seriously)

The theme of the App Platform is to accelerate developer output. And if you're emailing a coworker, you're not making much output. What if we were able to register an API into a self-service portal, and we could let everyone browse that API at their own pace. And what if we could test out that API without having to set up our own testing harness? What if we could do it right in the browser?

Read on.

## App Service API Portal
Head over to [the API Portal]({{< param tap_gui_url >}}/api-docs) and you'll see that we've registered a number of sample APIs.

![api portal](/images/api-portal-listing.png?classes=border,shadow)

You can test any of these APIs in your browser. But for this workshop, let's try the Spotify API. Click on that entry, then click on the "Definition" tab in the resulting page.
![api portal](/images/api-portal-spotify-top.png?classes=border,shadow)

If you're familiar with Swagger, this should look familiar to you. Because it _is_ Swagger. The API Portal supports a number of frameworks. We're just choosing this one because it's one of the more popular frameworks.

If you scroll down to any of the API calls, you can test out the API in your browser, just as you do with Swagger.
![api portal](/images/api-portal-execute.png?classes=border,shadow)



