+++
archetype = "chapter"
title = "Getting Productive"
weight = 3
+++

In this chapter, we'll go beyond the basics and show what life is like when iterating on our code. We'll also get into Dockerfile support.