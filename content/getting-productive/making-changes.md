---
title: "Making Changes"
date: 2022-08-16T16:56:48-04:00
draft: false
weight: 2
---

Now that our own copy of the code is deployed, let's walk through the process of making changes.

As a reminder, the way that we deployed our code was to effectively 'register' a git repo+branch. So the way we update our code on the platform is by updating our code in the repo.

## Modify the Code and Push
Find the file `src/main/java/com/example/springboot/HelloController.java` and edit the return value.

Change this:
```java
        public String index() {
                return "Greetings from Spring Boot + Tanzu!";
        }
```

To this:
```java
        public String index() {
                return "Whatever text makes you feel good";
        }
```

After making those changes, commit them and push your code to your repo.
```sh
git commit -am 'simple change'
git push origin main #or git push origin master
```

And tail the app again:
```sh
tanzu -n $USERNAME app workload tail $USERNAME-app
```

The App Platform will detect that a new version of the code has been deployed and will pull it down and run it through the Supply Chain again. When it's finished, you can view your updated code by hitting 'refresh' in your browser.

Next, let's talk about everyone's favorite topic - Security.