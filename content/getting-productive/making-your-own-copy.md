---
title: "Making Your Own Copy"
date: 2022-08-16T14:29:57-04:00
draft: false
weight: 1
---

## Iterating On Our App
Up until this point, we've been working on _the same repo_. What we need to do now, is clean up, and make a new copy of the code just for our personal use.

## Clean Up
Let's delete our workload with the following:
```
tanzu -n $USERNAME app workload delete $USERNAME-app
```

## Make Our Own Copy
Using your favorite Git platform (i.e. Github or Gitlab), create a new project (be sure to make it public) and push the contents of your Accelerator to that repo (this can vary depending on your platform, ask around for help if needed). 

At the end, you should have your own git repo with the contents of the Accelerator.

## Update Our Workload File and Deploy
Now that we have a new source for our code (our own repo, instead of the original one), we need to update our `workload.yaml` file to point to that new repo.

Again, modify `config/workload.yaml`:

Change this:
```yml
  source:
    git:
      url: https://github.com/sample-accelerators/tanzu-java-web-app
```

To this:
```yml
  source:
    git:
      url: https://YOUR-OWN-PLATFORM/YOUR-OWN-REPO/tanzu-java-web-app
```

Don't forget to change the name of the app, just like you did with the Accelerator:
```yaml
metadata:
  name: user-05-app
```

And with that, we can now deploy our own code to the platform with the same command:
```
tanzu -n $USERNAME app workload create -f config/workload.yaml
```

Confirm that we have success by fetching the URL with:
```
tanzu -n $USERNAME app workload get $USERNAME-app
```

Also, you can view the [workloads dashboard]({{< param tap_gui_url>}}/supply-chain) to view your app's journey through the Supply Chain and to troubleshoot, if needed.