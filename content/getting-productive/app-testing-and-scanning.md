---
title: "App Testing and Scanning"
date: 2022-08-17T10:18:10-04:00
draft: false
weight: 3
---

In this section, we're going to update our workload to include testing and scanning. While the tests and scans run, we'll explain what's happening.

## Update Your Workload
Back in our `workload.yaml`, we need to add one line. It's an annotation that tells the Platform we'd like to include scans and tests.

Before:
```yml
apiVersion: carto.run/v1alpha1
kind: Workload
metadata:
  labels:
    app.kubernetes.io/part-of: tanzu-java-web-app
    apps.tanzu.vmware.com/workload-type: web
```

After:
```yml
apiVersion: carto.run/v1alpha1
kind: Workload
metadata:
  labels:
    app.kubernetes.io/part-of: tanzu-java-web-app
    apps.tanzu.vmware.com/workload-type: web
    apps.tanzu.vmware.com/has-tests: true      <----- add this line
```

Now let's update our app:
```sh
tanzu -n $USERNAME app workload update -f config/workload.yaml
```
While that's running, let's explain what just happened.

## A Word on Supply Chains
![filter by pod](/images/supply-chain-logos.png?classes=border,shadow)

In order for an app to make it from your workstation to a Kubernetes cluster, a lot of things must happen. We briefly covered this in the [Introduction]({{< ref "introduction/deploying-with-containers.md">}}). In the introduction, we also said that the Platform takes care of a lot of this work for us. Supply Chains are how this is done. The App Platform ships with a few of these out of the box. They are named, appropriately, Out of The Box (or OOTB) Supply Chains.

In the Application Platform, a Supply Chain is a coordinated series of actions taken to objects inside a Kubernetes cluster, coordinated by the open source project [Cartographer](https://cartographer.sh/). At first glance, Cartographer is not unlike a CI/CD pipeline that we've come to know and love but the difference here is that everything is _Kubernetes native_. We don't have to coordinate SSH'ing to servers, or spawning container images and linking them together in a disparate fashion. Because everything happens _inside the Kubernetes cluster_, we have a much more seamless ecosystem. 

Can Supply Chains reach services outside the Kubernetes cluster? Absolutely. But you're losing out on the goodness of Kubernetes when you do that. 

That is not to say that Supply Chains are an all-or-nothing thing. If you're working with (say) Gitlab-CI today, _continue using that_. What Supply Chains do is help minmize the work of getting your app into Kubernetes. As we said earlier, there are several types that ship out of the box, the simplest one only being responsible for deploying an app. Some of the more involving Supply Chains include testing and scanning.

Which brings us back to our app that we updated.

## Source Code Testing with Tekton
Find your app again in the [workloads dashboard]({{< param tap_gui_url >}}/supply-chain) and notice now, how there are more tasks. We've added the following:
- code testing
- container image scanning

Click on the Source Tester task, then on the test ID and you can view the testing logs/results.

![code test results](/images/supply-chain-click-on-testing.png?classes=border,shadow&width=35pc)

Did your app fail the test? Why did it fail?

![code test failure](/images/supply-chain-test-failed.png?classes=border,shadow)

Go back and undo your edits to the source code so it passes the test

![code test results](/images/supply-chain-test-result.png?classes=border,shadow)
This testing was done with a Tekton pipeline that lives in your developer Kubernetes namespace. To see that pipeline, you can fetch the pipelines in your namespace, then get the yaml output.

```sh
# NOTE: Your pipeline may have a different name

kubectl get pipeline developer-defined-tekton-pipeline -n $USERNAME -o yaml
```

The task that ran your test can be found toward the bottom of that output. In this case, it's running a `./mvn test`. 
```yml
spec:
  params:
  - name: source-url
    type: string
  - name: source-revision
    type: string
  tasks:
  - name: test
    params:
    - name: source-url
      value: $(params.source-url)
    - name: source-revision
      value: $(params.source-revision)
    taskSpec:
      metadata: {}
      params:
      - name: source-url
        type: string
      - name: source-revision
        type: string
      spec: null
      steps:
      - image: gradle
        name: test
        resources: {}
        script: |-
          cd `mktemp -d`

          wget -qO- $(params.source-url) | tar xvz -m
          ./mvnw test
```
Also worth noting is the `wget` line. Notice that the task is pulling code from a location managed by Cartographer (`$(params.source-url)`). This means our Tekton task doens't need to be concerned with how to fetch our code - Cartographer took care of that for us (when we ran our `tanzu app create`). We just focus on implementing our test.

The creation and management of custom Supply Chains is a topic for another workshop. For right now, we'll stick with the Out of the Box Supply Chains. 

Let's see our container scan results.

## Container Scan Results with Grype
Back in the Supply Chain view for our app, click on the Image Scanner task:

![container scan results](/images/supply-chain-container-scan-results-fail.png?classes=border,shadow)

It's possible that our container has failed the scan. If that's the case, the Supply Chain _does not proceed any further_. Additionally, we get a list of the CVE findings. And if we click on one, we get linkouts to find more details about those CVEs so we know what version we need to update to.

![cve details](/images/cve-details.png?classes=border,shadow)

Why did the scan fail? Because our policy said we don't allow CVE findings of severity `high` or worse. To see that scan policy, you can look at the `scanPolicy` object in your dev namespace:
```sh
kubectl get scanpolicy -n $USERNAME -o yaml
```

Skipping down to the spec, we can see the [Regofile](https://www.openpolicyagent.org/docs/latest/policy-language/) used to define our policy. As a reminder, this is all part of the Out of the Box supply chains. You get these resources automatically. You are not on the hook for maintaining them.

```yml
  spec:
    regoFile: |
      package main

      # Accepted Values: "Critical", "High", "Medium", "Low", "Negligible", "UnknownSeverity"
      notAllowedSeverities := ["Critical","UnknownSeverity","High"]
      ignoreCves := []
```

Also see how we have the `ignoreCves` variable. This allows us to build an "allow" list of sorts, because Security is always a nuanced discussion and sometimes you have a 'high' CVE with no known mitigation yet.

## Shifting Security to the Left
Thinking for a moment on what we just saw. Just by adding a single line to our `workload.yaml` file, we were able to trigger a series of testing and scanning for our app automatically. And in doing, we got _immediate_ feedback on our apps security posture. We got test results, we got container scanning, we even got remediation advice. In most organizations, this is a months-long process of submitting your app to Security (with a capital 'S'), waiting in their queue, letting them scan it, receiving a spreadsheet, etc and so on. You get the idea.

When we take a security process that usually takes months. And we shrink it down to something that takes minutes. _That_ is what people are talking about when they say "shifting security to the left".