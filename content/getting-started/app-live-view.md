---
title: "App Live View"
date: 2022-08-16T14:32:02-04:00
draft: false
weight: 3
---

## Viewing Your App

Deploying an app is only the start. Let's examine some details about what we just launched. 

Point your browser over to [{{< param tap_gui_url >}}/catalog/default/component/tanzu-java-web-app](http://tap-gui.tap.tacticalprogramming.com/catalog/default/component/tanzu-java-web-app) and click on "Runtime Resources". Under the 'Kind' field, choose `Pod` to narrow down the resources. Click on the pod named for your app.

![filter by pod](/images/app-live-view-filter-by-pod.png?classes=border,shadow)

From there, you can see everything available to you, including:
- container logs
- disk/memory consumption
- health check endpoints
- HTTP request/response details
- environment variables
- and more

![filter by pod](/images/app-live-view-overview.png?classes=border,shadow)

Next, we'll look at what it looks like to iterate on our code.