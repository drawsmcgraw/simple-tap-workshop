---
title: "Logging Into K8s"
date: 2022-08-23T15:28:25-04:00
draft: false
weight: 1
---

Because the Tanzu App Platform is deployed into a Kubernetes cluster, logging into TAP is two-part:
1. Logging into your Kubernetes cluster
1. Logging into the TAP web UI.

On this page, we'll log into our k8s cluster. On the next page, we'll log into TAP.

## Logging Into Your Kubernetes Cluster
We will download a `kubeconfig` file and place it at `~/.kube/config`

Download the kubeconfig [here](/tap-workshop-kubeconfig). 

Use the following commands to back up any existing kubeconfig and drop in the kubeconfig for this workshop:
```sh
cp ~/.kube/config{,.bak}
mv /path/to/downloaded/kubeconfig ~/.kube/config
```

Once in place, issue any `kubectl` command to prompt a login. We will log in with our browsers:
```sh 
kubectl get pods

Log in by visiting this link:

    https://a153ffb69c49945d783601e6fc875e2c-1265930880.us-east-2.elb.amazonaws.com/oauth2/authorize?access_type=offline&client_id=pinniped-cli&code_challenge=vXNljCgAYlfiWrjwIUoCZjzjLZ-bfs00FAf2EjJzueE&code_challenge_method=S256&nonce=f1aac39b1efd957af6770b0100bdd537&redirect_uri=http%3A%2F%2F127.0.0.1%3A44133%2Fcallback&response_mode=form_post&response_type=code&scope=offline_access+openid+pinniped%3Arequest-audience&state=f81858173ec618db35723593c5dc89fb

    Optionally, paste your authorization code: [...]
```

Log in with credentials provided by the facilitator. At first, you will get a `Forbidden` message. That's because you need to specify your k8s namespace.

## Set Up Aliases
We _strongly_ recommend aliasing `kubectl` to include your namespace as well, such as:
```sh
export USERNAME=user-02
alias kubectl='kubectl -n $USERNAME'
```

While we're here, let's do the same thing for the `tanzu` cli:
```sh
alias tanzu='tanzu -n $USERNAME'
```

The examples in this workshop will include the use of this environment variable for easy copy/pasting.