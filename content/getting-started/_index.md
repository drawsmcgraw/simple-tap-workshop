+++
archetype = "chapter"
title = "Getting Started"
weight = 2
+++

In this section, we get our first hands-on experience with TAP and deploy our first application.