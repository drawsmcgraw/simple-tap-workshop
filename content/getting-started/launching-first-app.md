---
title: "Launching Your First App"
date: 2022-08-16T10:29:03-04:00
draft: false
weight: 2
---

## Logging Into the TAP UI

Point your browser to [{{< param tap_gui_url >}}]({{< param tap_gui_url >}}). Depending on your environment provided, you'll be logging in either as a guest or with our own account created for us.

## Downloading Your First Accelerator

### Download Via Browser
Once you've logged in, follow the "Create" link on the left side to see the available Accelerators. For this exercise, we'll go with the Tanzu Java Web App.

If you're asked for the container image repository, enter `{{< param container_registry>}}`.
Generate the accelerator, download it to your workspace on your workstation, and unpack it.

### Download Via Command Line
Alternatively, you can use the `tanzu` CLI to download the Accelerator. Use the following command:

```sh
tanzu accelerator generate tanzu-java-web-app \
--options '{"repositoryPrefix":"harbor.tanzu.tacticalprogramming.com"}' \
--server-url http://accelerator.{{< param tap_base_domain>}}
```

Unpack the zip file into your workspace.

## Deploying
In your terminal, navigate to where you unpacked the Accelerator. We need to make one edit, to change the name of the app, before we can deploy.

Edit the file: `tanzu-java-web-app/config/workload.yaml`

Change this:
```yml
apiVersion: carto.run/v1alpha1
kind: Workload
metadata:
  name: tanzu-java-web-app
```

To this:
```yml
apiVersion: carto.run/v1alpha1
kind: Workload
metadata:
  name: $USERNAME-app
```
Where `$USERNAME` is your username.

Now, we can deploy with:
```sh
tanzu -n $USERNAME app workload create -f config/workload.yaml
```
Where `$USERNAME` is the Kubernetes namespace you're using.

You should see something like the following (answer 'yes' to the prompt):
```sh
Create workload:
      1 + |---
      2 + |apiVersion: carto.run/v1alpha1
      3 + |kind: Workload
      4 + |metadata:
      5 + |  labels:
      6 + |    app.kubernetes.io/part-of: tanzu-java-web-app
      7 + |    apps.tanzu.vmware.com/workload-type: web
      8 + |  name: silent-bob-app
      9 + |  namespace: dev-tap
     10 + |spec:
     11 + |  params:
     12 + |  - name: annotations
     13 + |    value:
     14 + |      autoscaling.knative.dev/minScale: "1"
     15 + |  source:
     16 + |    git:
     17 + |      ref:
     18 + |        branch: main
     19 + |      url: https://github.com/sample-accelerators/tanzu-java-web-app

? Do you want to create this workload? (y/N)
```

## Viewing the Deployment
After deploying, you can view the build process two ways:
1. With the `tanzu` CLI
1. In the web UI


## Tanzu CLI
Run the following to view the deployment in your terminal:

```sh
tanzu app workload tail $USERNAME-app
```

## With a Browser
Point your browser to [{{< param tap_gui_url>}}/supply-chain](http://tap-gui.tap.tacticalprogramming.com/supply-chain) and find your app in the list of Workloads. Click the name to show the app going through the Supply Chain (more on Supply Chains in a little bit).

## Viewing the App
Finally, when your app has finished deploying, you can fetch details about it (including the URL where you can visit it) by running the following:

```sh
tanzu -n $USERNAME app workload get $USERNAME-app
```

You should see output similar to the following:
```sh
# tanzu-java-web-app-no-testing: Ready
---
lastTransitionTime: "2022-08-15T21:50:58Z"
message: ""
reason: Ready
status: "True"
type: Ready

Pods
NAME                                                              STATUS      RESTARTS   AGE
tanzu-java-web-app-no-testing-00002-deployment-55d9fbdcbb-dlg2n   Running     0          19h
tanzu-java-web-app-no-testing-build-1-build-pod                   Succeeded   0          7d
tanzu-java-web-app-no-testing-build-2-build-pod                   Succeeded   0          19h
tanzu-java-web-app-no-testing-config-writer-lsd5v-pod             Succeeded   0          19h
tanzu-java-web-app-no-testing-config-writer-rc5f6-pod             Succeeded   0          7d

Knative Services
NAME                            READY   URL
tanzu-java-web-app-no-testing   Ready   http://tanzu-java-web-app-no-testing.dev-tap.tap.tacticalprogramming.com
```

Copy/paste that URL in a browser and view your app!

Next, we'll show how the App Platform uses a gitops workflow to automatically deploy changes to your app.